export default function Footer() {
  return (
    <footer className="footing text-center d-flex d-inline justify-content-center d-grid gap-3 py-3">
      <div className="d-inline text-dark">AromaEmperium By Jonathan Abad</div>
      <div className="d-inline text-dark mx-3">|</div>
      <div className="d-inline text-dark">© Copyright 2023</div>
    </footer>
  );
}
