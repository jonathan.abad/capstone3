import { ListGroup, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebook,
  faInstagram,
  faTwitter,
  faGithub,
} from "@fortawesome/free-brands-svg-icons";
export default function Section() {
  return (
    <Row className="my-5">
      <Col md={5}>
        <div className="text-center section-title">
          <h1>AromaEmperium</h1>
          <h5>Sophisticated simplicity for the independent mind.</h5>
          <div className="social-button d-inline mx-1 text-dark secondary-color">
            <FontAwesomeIcon icon={faFacebook} />
          </div>
          <div className="social-button d-inline mx-1 text-dark secondary-color">
            <FontAwesomeIcon icon={faInstagram} />
          </div>
          <div className="social-button d-inline mx-1 text-dark secondary-color">
            <FontAwesomeIcon icon={faTwitter} />
          </div>
          <div className="social-button d-inline mx-1 text-dark secondary-color">
            <FontAwesomeIcon icon={faGithub} />
          </div>
        </div>
      </Col>
      <Col md={2}>
        <div className="customer-service">
          <h4 className="section-cc">Customer Care</h4>
          <ListGroup variant="flush">
            <ListGroup.Item>Contact Us</ListGroup.Item>
            <ListGroup.Item>FAQs</ListGroup.Item>
            <ListGroup.Item>Shipping Information</ListGroup.Item>
            <ListGroup.Item>Returns & Exchanges</ListGroup.Item>
          </ListGroup>
        </div>
      </Col>
      <Col md={2} className="section2">
        <ListGroup variant="flush">
          <ListGroup.Item>About Us</ListGroup.Item>
          <ListGroup.Item>Our Story</ListGroup.Item>
          <ListGroup.Item>Quality Guarantee</ListGroup.Item>
        </ListGroup>
      </Col>

      <Col md={3}>
        <div>
          <h4 className="section-newsletter">Stay Updated</h4>
          <h4>Discover New Scents</h4>
          <h6>Be the first to know about our latest perfumes and offers.</h6>
        </div>
      </Col>
    </Row>
  );
}
